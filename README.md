
# SIDLOC MCU

## Implementations

Each implementation resides on its own branch, the following are under development so far:

* Ariane-6 ([Ariane-6](https://gitlab.com/librespacefoundation/sidloc/sidloc-mcu/-/tree/Ariane-6))
* Qubik ([qubik](https://gitlab.com/librespacefoundation/sidloc/sidloc-mcu/-/tree/qubik))
* 1U Cubesat externally attached ([cubesat-external](https://gitlab.com/librespacefoundation/sidloc/sidloc-mcu/-/tree/cubesat-external))
* PQ9ISH (master)